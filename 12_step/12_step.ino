// P I R
//the time we give the sensor to calibrate (10-60 secs according to the datasheet)
int calibrationTime = 30;        

//the time when the sensor outputs a low impulse
long unsigned int lowIn;         

//the amount of milliseconds the sensor has to be low 
//before we assume all motion has stopped
long unsigned int pause = 50;  

boolean lockLow = true;
boolean takeLowTime;  

int pirPin = A0;    //the digital pin connected to the PIR sensor's output

// END PIR

byte bulbX = 0;
byte bulb0 = 8;
byte bulb1 = 9;
byte bulb2 = 10;
byte bulb3 = 11;
byte bulb4 = 12;
byte bulb5 = 13;
byte bulb6 = 2;
byte bulb7 = 3;
byte bulb8 = 4;
byte bulb9 = 5;
byte bulb10 = 6;
byte bulb11 = 7;

byte currentBulb = 0;
byte currentSequence = 0;
int onTime;
int waitBeforeNext = 10000;

const byte numSequences = 25;
const byte numKeyframes = 14;

byte randomSequenceIndices[numSequences];
byte randomSequenceIndex = numSequences;

boolean motionDetected = false;


int sequenceArray[numSequences][numKeyframes*2] = {
{bulb0,500,bulb1,500,bulb2,500,bulb3,533,bulb4,467,bulb5,500,bulb6,500,bulb7,500,bulb8,533,bulb9,533,bulb10,500,bulb11,533,bulbX,0,bulbX,0},
{bulb0,800,bulb1,733,bulb2,800,bulb3,767,bulb4,767,bulb5,900,bulb6,1367,bulb5,1567,bulb4,667,bulb3,2067,bulb2,3267,bulb1,667,bulb0,633,bulbX,0},
{bulb0,667,bulb1,500,bulb2,667,bulb3,567,bulb4,567,bulb5,567,bulb6,567,bulb7,567,bulb8,600,bulb9,767,bulb10,1467,bulb11,867,bulbX,0,bulbX,0},
{bulb0,767,bulb1,1067,bulb2,1000,bulb3,700,bulb4,633,bulb5,667,bulb6,633,bulb7,600,bulb8,600,bulb9,700,bulb10,667,bulb11,1033,bulbX,0,bulbX,0},
{bulb0,767,bulb1,700,bulb2,2533,bulb3,4333,bulb4,1200,bulb5,833,bulb6,700,bulb7,633,bulb8,633,bulb9,633,bulb10,700,bulb11,700,bulbX,0,bulbX,0},
{bulb0,1300,bulb1,867,bulb2,900,bulb3,800,bulb4,700,bulb5,667,bulb6,633,bulb7,900,bulb8,1500,bulb9,967,bulb10,1033,bulb11,1000,bulbX,0,bulbX,0},
{bulb0,1167,bulb1,933,bulb2,1033,bulb3,1067,bulb4,1000,bulb5,1033,bulb6,1100,bulb7,1167,bulb8,1167,bulb9,1900,bulb10,1500,bulb11,1200,bulbX,0,bulbX,0},
{bulb0,200,bulb1,300,bulb2,300,bulb3,300,bulb4,333,bulb5,367,bulb6,400,bulb7,433,bulb8,500,bulb9,467,bulb10,500,bulb11,467},
{bulb0,667,bulb1,700,bulb2,700,bulb3,733,bulb4,700,bulb5,700,bulb6,700,bulb7,667,bulb8,700,bulb9,733,bulb10,667,bulb11,733},
{bulb0,667,bulb1,733,bulb2,700,bulb3,667,bulb4,767,bulb5,667,bulb6,633,bulb7,667,bulb8,600,bulb9,567,bulb10,667,bulb11,633,bulbX,0,bulbX,0},
{bulb0,367,bulb1,400,bulb2,333,bulb3,300,bulb4,300,bulb5,300,bulb6,267,bulb7,300,bulb8,233,bulb9,267,bulb10,233,bulb11,267,bulbX,0,bulbX,0},
{bulb0,833,bulb1,667,bulb2,633,bulb3,567,bulb4,567,bulb5,567,bulb6,567,bulb7,600,bulb8,633,bulb9,700,bulb10,833,bulb11,900,bulbX,0,bulbX,0},
{bulb0,1000,bulb1,733,bulb2,767,bulb3,733,bulb4,633,bulb5,767,bulb6,667,bulb7,667,bulb8,667,bulb9,700,bulb10,667,bulb11,667,bulbX,0,bulbX,0},
{bulb0,633,bulb1,667,bulb2,700,bulb3,700,bulb4,700,bulb5,633,bulb6,667,bulb7,667,bulb8,700,bulb9,700,bulb10,733,bulb11,1000,bulbX,0,bulbX,0},
{bulb0,600,bulb1,633,bulb2,600,bulb3,600,bulb4,633,bulb5,633,bulb6,667,bulb7,800,bulb8,933,bulb9,867,bulb10,733,bulb11,633},
{bulb0,733,bulb1,633,bulb2,633,bulb3,500,bulb4,433,bulb5,333,bulb6,300,bulb7,267,bulb8,300,bulb9,300,bulb10,267,bulb11,300},
{bulb0,200,bulb1,300,bulb2,300,bulb3,300,bulb4,333,bulb5,367,bulb6,400,bulb7,433,bulb8,500,bulb9,467,bulb10,500,bulb11,467},
{bulb0,667,bulb1,667,bulb2,700,bulb3,633,bulb4,733,bulb5,733,bulb6,700,bulb7,733,bulb8,767,bulb9,767,bulb10,867,bulb11,1033,bulbX,0,bulbX,0},
{bulb0,867,bulb1,833,bulb2,600,bulb3,633,bulb4,633,bulb5,700,bulb6,733,bulb7,800,bulb8,867,bulb9,1167,bulb10,1000,bulb11,800,bulbX,0,bulbX,0},
{bulb0,900,bulb1,1000,bulb2,933,bulb3,700,bulb4,700,bulb5,700,bulb6,733,bulb7,633,bulb8,600,bulb9,567,bulb10,600,bulb11,600,bulbX,0,bulbX,0},
{bulb0,433,bulb1,433,bulb2,467,bulb3,433,bulb4,467,bulb5,467,bulb6,433,bulb7,467,bulb8,433,bulb9,467,bulb10,433,bulb11,433},
{bulb0,433,bulb1,433,bulb2,467,bulb3,467,bulb4,467,bulb5,500,bulb6,433,bulb7,467,bulb8,500,bulb9,500,bulb10,467,bulb11,467},
{bulb0,433,bulb1,467,bulb2,500,bulb3,500,bulb4,567,bulb5,500,bulb6,500,bulb7,500,bulb8,500,bulb9,433,bulb10,500,bulb11,500},
{bulb0,600,bulb1,600,bulb2,567,bulb3,567,bulb4,600,bulb5,633,bulb6,600,bulb7,600,bulb8,567,bulb9,600,bulb10,600,bulb11,600},
{bulb0,400,bulb1,367,bulb2,433,bulb3,400,bulb4,400,bulb5,367,bulb6,400,bulb7,400,bulb8,367,bulb9,367,bulb10,400,bulb11,400},
};

void setup() {
  Serial.begin(9600);
  
  pinMode(pirPin, INPUT);
  
  pinMode(bulb0, OUTPUT); 
  pinMode(bulb1, OUTPUT); 
  pinMode(bulb2, OUTPUT); 
  pinMode(bulb3, OUTPUT); 
  pinMode(bulb4, OUTPUT); 
  pinMode(bulb5, OUTPUT); 
  pinMode(bulb6, OUTPUT); 
  pinMode(bulb7, OUTPUT); 
  pinMode(bulb8, OUTPUT); 
  pinMode(bulb9, OUTPUT); 
  pinMode(bulb10, OUTPUT); 
  pinMode(bulb11, OUTPUT);
  
  digitalWrite(bulb0, HIGH); 
  digitalWrite(bulb1, HIGH); 
  digitalWrite(bulb2, HIGH); 
  digitalWrite(bulb3, HIGH); 
  digitalWrite(bulb4, HIGH); 
  digitalWrite(bulb5, HIGH); 
  digitalWrite(bulb6, HIGH); 
  digitalWrite(bulb7, HIGH); 
  digitalWrite(bulb8, HIGH); 
  digitalWrite(bulb9, HIGH); 
  digitalWrite(bulb10, HIGH); 
  digitalWrite(bulb11, HIGH);

  
    //give the sensor some time to calibrate
  Serial.print("calibrating sensor ");
    for(int i = 0; i < calibrationTime; i++){
      Serial.print(".");
      delay(1000);
      }
    Serial.println(" done");
    Serial.println("SENSOR ACTIVE");
    delay(50);
    
   randomSequenceIndex = numKeyframes;
   
   for(int i=0; i < numKeyframes; i++) {
      randomSequenceIndices[i] = i;
    }   
    
    randomSequenceIndex = 0;
    shuffle(randomSequenceIndices, numSequences);  
    
    Serial.println("Shuffled sequences");
}


void loop() {
  
float sensorValue = analogRead(pirPin) * (5.0 / 1023.0);
  
  if(sensorValue >= 1){
     
       motionDetected = true;
        Serial.print("Motion detected at "); Serial.print(millis()/1000); Serial.print(" secs. A0 = ");
        Serial.println(sensorValue);
       
       if(lockLow){  
         //makes sure we wait for a transition to LOW before any further output is made:
         lockLow = false;            
         delay(50);
         }         
         takeLowTime = true;
      
     } else  if(sensorValue < 1) {

       motionDetected = false;

       if(takeLowTime){
        lowIn = millis();          //save the time of the transition from high to LOW
        takeLowTime = false;       //make sure this is only done at the start of a LOW phase
        }
       //if the sensor is low for more than the given pause, 
       //we assume that no more motion is going to happen
       if(!lockLow && millis() - lowIn > pause){  
           //makes sure this block of code is only executed again after 
           //a new motion sequence has been detected
           lockLow = true;                        
           delay(50);
           }
       }

  
////////////////
  
if(motionDetected) {
  
  Serial.print("current Sequence = ");
  Serial.println(currentSequence);
  
  int randomDirection = random(2);
  
  
  if(randomDirection == 0) {
    for(int i = 0; i < numKeyframes*2; i+=2) {
         
      currentBulb = sequenceArray[currentSequence][i];
      onTime = sequenceArray[currentSequence][i + 1];
      
    Serial.println(currentBulb);
    
      digitalWrite(currentBulb, LOW);
      delay(onTime);
      digitalWrite(currentBulb, HIGH);      
    }
  }
  
  if(randomDirection == 1) {
    
   for(int i = numKeyframes*2-2; i > -1; i-=2) {     
    
      currentBulb = sequenceArray[currentSequence][i];
      onTime = sequenceArray[currentSequence][i+1];
      
    Serial.println(currentBulb);
    
      digitalWrite(currentBulb, LOW);
      delay(onTime);
      digitalWrite(currentBulb, HIGH);      
    }
  }
  
  if(randomSequenceIndex >= numSequences) 
  {
    randomSequenceIndex = 0;
    shuffle(randomSequenceIndices, numSequences);  
    
    Serial.println("Shuffled sequences");
  }
  
  currentSequence = randomSequenceIndices[randomSequenceIndex];
  randomSequenceIndex++;

  delay(waitBeforeNext);

  }
 
  
  
}


void shuffle (byte* randNum, int n)
  {
  while (--n >= 2)
    {
    // n is now the last pertinent index
    byte k = random (n); // 0 <= k <= n - 1
    // Swap
    byte temp = randNum[n];
    randNum[n] = randNum[k];
    randNum[k] = temp;
    }
}  // end of shuffle

